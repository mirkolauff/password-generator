import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  length = 10;
  includeLetters = true;
  includeNumbers = true;
  includeSymbols = true;
  password = '';

  onChangeLength(value: string) {
    const parsedValue = parseInt(value);
    this.length = !isNaN(parsedValue) ? parsedValue : 0;
  }

  onChangeUseLetters() {
    this.includeLetters = !this.includeLetters;
  }

  onChangeUseNumbers() {
    this.includeNumbers = !this.includeNumbers;
  }

  onChangeUseSymbols() {
    this.includeSymbols = !this.includeSymbols;
  }

  onButtonClick() {
    const numbers = '1234567890';
    const letters = 'abcdefghijklmnopqrstuvwxyz';
    const symbols = '!@#$%^&()_';

    let validChars = '';
    if (this.includeNumbers) validChars += numbers;
    if (this.includeLetters) validChars += letters;
    if (this.includeSymbols) validChars += symbols;

    let generatedPassword = '';
    for (let i = 0; i < this.length; i++) {
      const index = Math.floor(Math.random() * validChars.length);
      generatedPassword += validChars[index];
    }

    this.password = generatedPassword;
  }
}
